import { StyleSheet, Text, View } from 'react-native';

export const Header = () => {

  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>Seguradora Avenida</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#04f',
    height: 70,
    width: '100%',
    textAlign: 'center',
    justifyContent: 'center',
  },
  headerText: {
    color: '#fff',
    marginLeft: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
})