import { useRef, useState } from "react";
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
  Text,
} from "react-native";
import SelectDropdown from "react-native-select-dropdown";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const Form = ({ navigation }) => {
  const dropdownRef = useRef({});

  const [client, setClient] = useState("");
  const [model, setModel] = useState("");
  const [selectedBrand, setSelectedBrand] = useState("");
  const [isRenewal, setIsRenewal] = useState(false);
  const [vehicleValue, setVehicleValue] = useState(null);
  const [insurancePrice, setInsurancePrice] = useState(null);
  const [list, setList] = useState([]);

  const brands = [
    "Chevrolet",
    "Chery",
    "Citroen",
    "Fiat",
    "Ford",
    "Honda",
    "Hyundai",
    "Jeep",
    "Peugeot",
    "Renault",
    "Volkswagem",
  ];

  const calculeValue = () => {
    if (!client || !model || !selectedBrand || !vehicleValue) {
      return alert("Preencha todos os campos!!!");
    }

    let insurance;
    if (!isRenewal) {
      insurance = (vehicleValue * 4.9) / 100;
    } else {
      insurance = (vehicleValue * 4.3) / 100;
    }
    setInsurancePrice(insurance.toFixed(2));
  };

  const clearForm = () => {
    setClient("");
    setModel("");
    dropdownRef.current.reset();
    setIsRenewal(false);
    setVehicleValue(null);
    setInsurancePrice(null);
  };

  const save = async (insurance) => {
    try {
      const jsonValue = JSON.stringify(insurance);
      await AsyncStorage.setItem("@storage_Key", jsonValue);
    } catch (err) {
      console.error(err);
    }
  };

  const register = async () => {
    const register = {
      id: new Date().getTime().toString(),
      client: client,
      model: model,
      brand: selectedBrand,
      isRenewal: isRenewal,
      vehicleValue: vehicleValue,
      insurancePrice: insurancePrice,
    };
    setList([...list, register]);
    save([...list, register]);
    clearForm();
    alert("Cotação Registrada com Sucesso!!");
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Cliente/Contato"
        style={styles.textInput}
        onChangeText={(e) => setClient(e)}
        value={client}
      />
      <TextInput
        placeholder="Modelo do Veículo"
        style={styles.textInput}
        onChangeText={(e) => setModel(e)}
        value={model}
      />
      <View style={styles.rowContainer}>
        <SelectDropdown
          data={brands}
          ref={dropdownRef}
          value={selectedBrand}
          onSelect={(selected) => setSelectedBrand(selected)}
          defaultButtonText="Selecione a Marca"
          defaultValue=""
          buttonStyle={styles.selectButton}
        />
        <BouncyCheckbox
          text="É renovação"
          disableBuiltInState
          isChecked={isRenewal}
          fillColor="#c4c4c4"
          iconStyle={{ borderRadius: 0 }}
          innerIconStyle={{ borderWidth: 2, borderRadius: 0 }}
          onPress={() => setIsRenewal(!isRenewal)}
          textStyle={{ textDecorationLine: "none" }}
        />
      </View>
      <TextInput
        placeholder="R$ Avaliação Tabela FIPE"
        keyboardType="number-pad"
        returnKeyType="done"
        style={styles.textInput}
        onChangeText={(e) => setVehicleValue(e)}
        value={vehicleValue}
      />
      <View style={styles.rowContainer}>
        <TouchableOpacity style={styles.button} onPress={() => calculeValue()}>
          <Text style={styles.textButton}>Calcular</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => clearForm()}>
          <Text style={styles.textButton}>Novo</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate("Interest Item");
          }}
        >
          <Text style={styles.textButton}>Listar</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.resultContainer}>
        {insurancePrice && (
          <Text style={styles.resultText}>
            Valor Estimado do Seguro R$: {insurancePrice}
          </Text>
        )}
        {insurancePrice && (
          <TouchableOpacity
            style={[styles.button, styles.resultButton]}
            onPress={() => register()}
          >
            <Text style={styles.textButton}>Registrar Interesse</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "90%",
    height: 100,
    justifyContent: "space-between",
    marginTop: 15,
  },
  textInput: {
    borderBottomColor: "#c4c4c4",
    borderBottomWidth: 1,
    height: 50,
  },
  rowContainer: {
    marginTop: 20,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  button: {
    width: 100,
    backgroundColor: "#DCDCDC",
    alignItems: "center",
    padding: 10,
    borderRadius: 5,
  },
  textButton: {
    fontWeight: "500",
  },
  selectButton: {
    borderRadius: 5,
    backgroundColor: "#DCDCDC",
  },
  resultContainer: {
    alignItems: "center",
    marginTop: 15,
  },
  resultText: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 15,
  },
  resultButton: {
    width: "80%",
  },
});
