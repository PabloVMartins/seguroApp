import { StyleSheet, Text, View } from "react-native"

export const ItemRow = ({label, value}) => {
  return (
    <View style={styles.row}>
      <Text style={[styles.textTitle, styles.textContent]}>{label}: </Text>
      <Text style={styles.textContent}>{value}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginVertical: 5,
  },
  textTitle: {
    fontWeight: 'bold',
  },
  textContent: {
    color: "#000",
    fontSize: 24,
  },
})