import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { ItemRow } from "./ItemRow";

export const InterestItem = () => {
  const [list, setList] = useState([]);

  const getData = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem("@storage_Key");
      const storageItem = jsonValue != null ? JSON.parse(jsonValue) : [];
      setList(storageItem);
    } catch (err) {
      console.error(err);
    }
  };

  const clearList = async () => {
    try {
      await AsyncStorage.removeItem("@storage_Key");
      setList([]);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView style={styles.content}>
        {list.length === 0 && (
          <Text style={styles.emptyList}>Não há Cotações</Text>
        )}
        {list.length > 0 && (
          <>
            {list?.map((item) => {
              const formatedVehicleValue = item.vehicleValue
                ? Number(item.vehicleValue)?.toFixed(2)
                : "";
              return (
                <View style={styles.card} key={item.id}>
                  <ItemRow label="Cliente" value={item.client} />
                  <ItemRow label="Modelo do Veículo" value={item.model} />
                  <ItemRow label="Marca" value={item.brand} />
                  <ItemRow
                    label="É Renovação"
                    value={item.isRenewal ? "Sim" : "Não"}
                  />
                  <ItemRow
                    label="Valor do Veículo R$"
                    value={formatedVehicleValue}
                  />
                  <ItemRow
                    label="Valor do Seguro R$"
                    value={item.insurancePrice}
                  />
                </View>
              );
            })}
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.button}
                onPress={() => clearList()}
              >
                <Text style={styles.textButton}>Limpar Lista</Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  content: {
    marginTop: 20,
    marginLeft: 15,
  },
  card: {
    paddingBottom: 20,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: "#04f",
    width: "95%",
  },
  buttonContainer: {
    width: 100,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    width: 100,
    backgroundColor: "#DCDCDC",
    alignItems: "center",
    padding: 10,
    borderRadius: 5,
  },
  textButton: {
    fontWeight: "500",
  },
  emptyList: {
    fontSize: 24,
    fontWeight: "bold",
    marginTop: 25,
  },
});
