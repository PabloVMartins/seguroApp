import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Form } from './Form';

export const Home = ({ navigation }) => {
  const [interestList, setInterestList] = useState([])
  const logo = require("../assets/seguroLogo.jpg")

  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.image} />
      <Form setInterestList={setInterestList} interestList={interestList} navigation={navigation} />
      <StatusBar style="auto" />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },  
  image: {
    width: 250,
    height: 100,
    margin: 15,
    alignSelf: "center",
    resizeMode: 'stretch'
  }
});