import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Home } from "./components/Home";
import { InterestItem } from "./components/InterestList";

export default function App() {
  const Stack = createNativeStackNavigator();

  const headerStyles = {
    headerStyle: {
      backgroundColor: "#04f",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  };

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomePage">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            title: "Seguradora Avenida - Home",
            ...headerStyles,
          }}
        />
        <Stack.Screen
          name="Interest Item"
          component={InterestItem}
          options={{
            title: "Seguradora Avenida - Cotações",
            ...headerStyles,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
